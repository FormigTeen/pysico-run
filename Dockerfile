FROM node:current-alpine

COPY ./app /var/www

WORKDIR /var/www

RUN cd /var/www && \
        apk --no-cache add tzdata ca-certificates && \
        npm install pm2@latest nodemon -g && \
        yarn install --prod

EXPOSE 3000

VOLUME /var/www

CMD ["pm2-runtime", "start", "npm", "--", "start"]
