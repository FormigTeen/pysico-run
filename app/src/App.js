import React from 'react';
import { IonPhaser } from '@ion-phaser/react'
import { store } from './Redux'


const App = () => {
    const { game } = store.getState()
    return (
        <IonPhaser game={{ ...game.config }} initialize={game.initialize} />
    )
}

export default App;
