import Phaser from 'phaser'

const Emitter = new Phaser.Events.EventEmitter();

export default Emitter;
