import { combineReducers, createStore, applyMiddleware } from 'redux'
import { default as game } from './reducers/Game.js'
import { default as grid } from './reducers/Grid.js'
import { default as player } from './reducers/Player.js'
import Emitter from './Emitter.js';

const reducer = combineReducers({
    game,
    grid,
    player
})

const eventTrigger = store => next => action => {
    let result = next(action)
    Emitter.emit(action.type)
    return result
}

const store = createStore(reducer, applyMiddleware(eventTrigger))

console.log(store.getState())

//store.subscribe(() => console.log(store.getState()))

export { store, reducer }
