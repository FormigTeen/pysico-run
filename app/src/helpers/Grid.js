import Phaser from 'phaser';
import { store } from '../Redux'
import { setCellAction } from '../reducers/Grid.js'

class Grid extends Phaser.GameObjects.Grid {
    constructor({ scene, rows, cols }) {
        const { grid } = store.getState();
        rows = rows ?? grid.rows;
        cols = cols ?? grid.cols;
        super(scene, 0, 0, scene.game.config.width, scene.game.config.height, scene.game.config.width / rows, scene.game.config.height / cols, 0xff0000)
        this.setOrigin(0, 0)
        this.setPosition(0, 0)
        scene.add.existing(this)
        this.updateStore()
    }

    updateStore() {
        store.dispatch(setCellAction({
            width: this.cellWidth,
            height: this.cellHeight
        }))
    }

}

export const getRowPos = (number) => {
    const { grid: { cell: { width } } } = store.getState();
    return width * ( number + 0.5 ) ;
}

export const getColPos = (number) => {
    const { grid: { cell: { height } } } = store.getState();
    return height * ( number + 0.5 );
}

export const getCell = () => {
    const { grid: { cell } } = store.getState();
    return cell;
}

export const getRowCount = () => {
    const { grid: { rows } } = store.getState();
    return rows;
}

export const getColCount = () => {
    const { grid: { cols } } = store.getState();
    return cols;
}

export const getColRandom = () => {
    return Math.floor(Math.random() * getColCount());
}

export const getRowRandom = () => {
    return Math.floor(Math.random() * getRowCount());
}




export default Grid
