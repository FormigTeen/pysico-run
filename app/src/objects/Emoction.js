
import Phaser from 'phaser';

class Emoction extends Phaser.GameObjects.Sprite {


    static onPreload({ scene, key }) {
        scene.load.spritesheet(key, process.env.PUBLIC_URL.concat('/resources/emoctions/SpriteSheet.png'), { frameWidth: 32, frameHeight: 32 })
    }

    constructor({ scene, key, x, y, type }) {
        super(scene, x, y, key)
        this.key = key
        this.type = type ?? 0
        scene.add.existing(this)
        this.setFrame(this.type)
    }

    setHeight (value) {
        this.setScale(value / this.height )
    }

}

export default Emoction
