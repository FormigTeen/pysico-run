import Phaser from 'phaser';
import Emoction from './Emoction'
import { store } from './../Redux'
import { setEffectAction, addLifeAction, addPointsAction, addBagAction } from '../reducers/Player.js'
import { getRowPos, getCell, getColPos, getColRandom, getRowRandom } from './../helpers/Grid';

class EmoctionGroup extends Phaser.GameObjects.Group {

    static onPreload({ scene, key }) {
        Emoction.onPreload({ scene, key })
        this.level = 0
    }

    constructor({ scene, key }) {
        super(scene, {
            defaultKey: key,
        })
        scene.add.existing(this)
    }

    addLevel() {
        this.level = this.level + 1;
    }

    spawn(col, row) {
        this.create({
            x: getRowPos(col), 
            y: getColPos(row),
            height: getCell().height * 0.7
        })
        return this
    }

    create({ x, y, type, height }) {
        const child = new Emoction({ 
            scene: this.scene, 
            key: this.defaultKey, 
            x: x, 
            y: y, 
            type: this.getKeyRandom()
        })
        child.setHeight(height)
        this.add(child)
    }

    collect({ x, y }) {
        this.getChildren().map( (child) => {
            if ( Math.floor(child.x) === Math.floor(x) && Math.floor(child.y) === Math.floor(y) ) {
                if ( isEffect(child.type) ) {
                    setEffect(child.type)
                    decreaseLife()
                } else {
                    addBag(child.type)
                    addPoints(1)
                    this.addLevel()
                }
                this.clear(true, true)
                this.spawn(getColRandom(), getRowRandom())
            }
            return child
        })
    }

    getKeyRandom(onlyBag = false) {
            const keys = onlyBag ? getBag() : [...Array(100).keys()].filter(_ => !getBag().includes(_));

        return keys[Math.floor(Math.random() * keys.length)];
    }

}


export const getBag = (value) => {
    const { player: { bag } } = store.getState()
    return bag
}

export const isEffect = (value) => {
    return getBag().indexOf(value) >= 0;
}

export const setEffect = (value) => {
    store.dispatch(setEffectAction(value))
}

export const decreaseLife = () => {
    store.dispatch(addLifeAction(-1))
}

export const addPoints = (value) => {
    store.dispatch(addPointsAction())
}

export const addBag = (value) => {
    store.dispatch(addBagAction(value))
}

export default EmoctionGroup
