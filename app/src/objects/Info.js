import Phaser from 'phaser';
import Emitter from '../Emitter.js'
import { addLifeAction, addPointsAction } from '../reducers/Player.js'
import { getRowPos } from '../helpers/Grid'
import { store } from './../Redux.js'

export default class Info extends Phaser.GameObjects.Text {
    constructor({ scene }) {
        super(scene, 0, 0, "Loading...", {
            fontSize: '25px',
            fontStyle: 'bold',
            backgroundColor: 'black',
        })
        this.reloadText()
        scene.add.existing(this)
        this.setEvents()
    }

    loadText () {
        return `Points: ${getPoints()}\t\tTime: 00:00\t\tLife: ${getLife()}`;
    }

    reloadText () {
        this.setText(this.loadText())
        this.setX(getRowPos(8) - this.width/2)
        this.setY(0)
        this.setOrigin(0, 0)
    }

    setEvents () {
        Emitter.on(addLifeAction, () => this.reloadText())
        Emitter.on(addPointsAction, () => this.reloadText())
    }

    resetEvents () {
        Emitter.off(addLifeAction)
        Emitter.off(addPointsAction)
    }

    destroy() {
        this.resetEvents();
        super.destroy();
    }
}

export const getPoints = () => {
    const { player: { points } } = store.getState();
    return points;
}

export const getLife = () => {
    const { player: { life } } = store.getState();
    return life;
}

