import Phaser from 'phaser'
import { store } from '../Redux'
import { getRowPos, getColPos, getCell } from '../helpers/Grid.js'
import { setDirectionAction, resetPointsAction } from '../reducers/Player.js'

const FILE_PATH = process.env.PUBLIC_URL.concat('/resources/players/common/sprite.png')

class Common extends Phaser.GameObjects.Sprite {
    
    static onPreload({ scene, key }) {
        scene.load.spritesheet(key, FILE_PATH, { frameWidth: 16, frameHeight: 15 })
    }

    constructor({ scene, key }) {
        super(scene, getRowPos(4), getColPos(4), key)
        this.key = key
        this.actionCounter = 1;
        scene.add.existing(this)
        this.setHeight(getCell().height)
        this.registerEvents()
        this.setAnims()
    }

    setAnims() {
        this.scene.anims.create({
            key: 'walk',
            frames: [
                { key: this.key, frame:8 },
                { key: this.key, frame:9 },
                { key: this.key, frame:10 },
                { key: this.key, frame:11 },
                { key: this.key, frame:12 },
                { key: this.key, frame:13 },
                { key: this.key, frame:14 },
                { key: this.key, frame:15 },
            ],
            frameRate: 32,
            repeat: -1
        });
        this.scene.anims.create({
            key: 'jump',
            frames: [
                { key: this.key, frame:0 },
                { key: this.key, frame:1 },
                { key: this.key, frame:2 },
                { key: this.key, frame:3 },
                { key: this.key, frame:4 },
                { key: this.key, frame:5 },
                { key: this.key, frame:6 },
                { key: this.key, frame:7 },
            ],
            frameRate: 32,
            repeat: -1
        });
    }

    setHeight (value) {
        this.setScale(value / this.height )
    }

    setWalkPosition()
    {
        switch (getDirection()) {
            case 0:
                this.play('jump', true)
                this.setY(this.y - getCell().height)
                break;
            case 1:
                this.play('walk', true)
                this.setFlipX(false)
                this.setX(this.x + getCell().height)
                break;
            case 2:
                this.play('jump', true)
                this.setY(this.y + getCell().height)
                break;
            case 3:
                this.play('walk', true)
                this.setFlipX(true)
                this.setX(this.x - getCell().height)
                break;
            default:
                break;
        }

    }

    walk() {
        if ( this.actionCounter % getVelocity() === 0 && this.isScreen() ) {
            this.setWalkPosition()
            this.onWalkFunc()
        }
        this.actionCounter = ( this.actionCounter % getVelocity() ) + 1
        return this
    }

    registerEvents() {
        this.scene.input.keyboard.on('keydown-W', () => setDirection(0));
        this.scene.input.keyboard.on('keydown-D', () => setDirection(1));
        this.scene.input.keyboard.on('keydown-S', () => setDirection(2));
        this.scene.input.keyboard.on('keydown-A', () => setDirection(3));
    }

    onUpdate() {
        this.walk()
    }

    onWalk(func) {
        this.onWalkFunc = func
    }

    isScreen() {
        if ( this.x < 0 || this.x > this.scene.game.config.width ) 
            return false

        if ( this.y < 0 || this.y > this.scene.game.config.height ) 
            return false

        return true
    }

}

export const getVelocity = () => {
    const { player: { velocity } } = store.getState();
    return velocity;
}

export const getDirection = () => {
    const { player: { direction } } = store.getState();
    return direction;
}

export const setDirection = (value) => {
    store.dispatch(setDirectionAction(value))
}

export const resetPoints = () => {
    store.dispatch(resetPointsAction())
}
export default Common
