import { createReducer } from '@reduxjs/toolkit'
import Start from './../scenes/Start'
import Phaser from 'phaser'

const initialState = {
    config: {
        width: 800,
        height: 800,
        type: Phaser.AUTO,
        scene: [Start]
    },
    initialize: true
}

const reducer = createReducer(initialState, {})

export default reducer
