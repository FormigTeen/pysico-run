import { createReducer, createAction } from '@reduxjs/toolkit'

const initialState = {
    rows: 17,
    cols: 17,
    cell: {
        width: 0,
        height: 0,
    }
}

export const setCellAction = createAction('grid/setCell');

const reducer = createReducer(initialState, {
    [setCellAction]: (state, { payload }) => ({ ...state, cell: payload })
})

export default reducer
