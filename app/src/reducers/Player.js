import { createReducer, createAction } from '@reduxjs/toolkit'

const initialState = {
    life: 5,
    points: 0,
    record: 0,
    effect: null,
    direction: 2,
    velocity: 10,
    bag: [],
}

export const addLifeAction = createAction('player/increaseLife');

export const setEffectAction = createAction('player/setEffect');

export const addPointsAction = createAction('player/addPoints');

export const resetPointsAction = createAction('player/resetPoints');

export const updateRecordAction = createAction('player/updateRecord');

export const setDirectionAction = createAction('player/setDirection');

export const addBagAction = createAction('player/addBag');

const reducer = createReducer(initialState, {

    [addLifeAction]: (state, { payload, type }) => {
        return ({ ...state, life: state.life + payload })
    },


    [addPointsAction]: (state, { payload, type }) => { 
        return ({ ...state, points: state.points + (payload ?? 1) }) 
    },

    [resetPointsAction]: (state, { type }) => { 
        return ({ ...state, points: 0 }) 
    },

    [updateRecordAction]: (state, { payload, type }) => { 
        return ({ ...state, record: ( state.record > state.points ? state.record : state.points )}) 
    },

    [setDirectionAction]: (state, { payload, type }) => { 
        return ({ ...state, direction: payload % 4 }) 
    },

    [addBagAction]: (state, { payload, type }) => { 
        return ({ ...state, bag: [...state.bag, payload]}) 
    },

    [setEffectAction]: (state, { payload, type }) => { 
        return ({ ...state, effect: payload }) 
    },

})

export default reducer
