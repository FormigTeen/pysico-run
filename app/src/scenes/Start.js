import Phaser from 'phaser';
import Common, { resetPoints } from './../players/Common';
import EmoctionGroup from './../objects/EmoctionGroup';
import Info from './../objects/Info';
import Grid, { getColRandom, getRowRandom } from './../helpers/Grid';

class Start extends Phaser.Scene {
    preload() {
        Common.onPreload({scene: this, key: 'common'})
        EmoctionGroup.onPreload({scene: this, key: 'emoction'})
    }

    create() {
        this.grid = new Grid({scene: this})
        this.info = new Info({scene: this});
        this.common = new Common({ 
            scene: this, 
            key: 'common',
        })

        this.emoctionGroup = new EmoctionGroup({ 
            scene: this, 
            key: 'emoction',
        }).spawn(getColRandom(), getRowRandom())

        this.common.onWalk(() => this.emoctionGroup.collect({x: this.common.x, y: this.common.y}))
    }

    restart() {
        this.scene.stop(this.scene.key);
        setTimeout(() => this.scene.start(this.scene.key), 2000);
    }

    update() {
        this.common.onUpdate();
        if ( !this.common.isScreen() ) {
            this.restart();
            resetPoints();
        }
    }

}

export default Start
